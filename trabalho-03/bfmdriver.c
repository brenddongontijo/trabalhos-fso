#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/interrupt.h>
#include <asm/io.h>

irq_handler_t irq_handler(int irq, void *dev_id, struct pt_regs *regs)
{
    static unsigned char scancode;

    scancode = inb(0x60);

    if(scancode == 0x01){
        printk("\nbfmdriver - Tecla Esc pressionada!");
    }
    else if(scancode == 0x02){
        printk("\nbfmdriver - Tecla 1 pressionada!");
    }
    else if(scancode == 0x03){
        printk("\nbfmdriver - Tecla 2 pressionada!");
    }
    else if(scancode == 0x04){
        printk("\nbfmdriver - Tecla 3 pressionada!");
    }
    else if(scancode == 0x05){
        printk("\nbfmdriver - Tecla 4 pressionada!");
    }
    else if(scancode == 0x06){
        printk("\nbfmdriver - Tecla 5 pressionada!");
    }
    else if(scancode == 0x07){
        printk("\nbfmdriver - Tecla 6 pressionada!");
    }
    else if(scancode == 0x08){
        printk("\nbfmdriver - Tecla 7 pressionada!");
    }
    else if(scancode == 0x09){
        printk("\nbfmdriver - Tecla 8 pressionada!");
    }
    else if(scancode == 0x0a){
        printk("\nbfmdriver - Tecla 9 pressionada!");
    }

    return (irq_handler_t) IRQ_HANDLED;
}

static int __init ini_module(void)
{
    printk("\nbfmdriver - Iniciando driver...");

    return request_irq(1, (irq_handler_t) irq_handler, IRQF_SHARED, "keyboard_irq_handler", (void *)(irq_handler));
}

static void __exit exi_module(void)
{
    printk ("\nbfmdriver - Finalizando driver !");
    free_irq(1, (void *)(irq_handler));
}

MODULE_LICENSE("GPL");

module_init(ini_module);

module_exit(exi_module);