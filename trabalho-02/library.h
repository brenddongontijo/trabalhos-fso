/* Bibliotecas Externas */
#include <pthread.h>
#include <semaphore.h>
#include <stdint.h>
#include <string.h>
#include <sys/shm.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

/* Bibliotecas Internas */
#include "macros.h"
#include "semaphores_library.h"
#include "shared_memory_library.h"

/******* Variáveis Globais *******/

/*
	Array de semáforos para controle dos parlamentares, onde:
	[0] = semáforo "binário" para controle dos senadores.
	[2] = semáforo mutex para controlar entrada/saída de deputados na sala de votação, onde seu valor inicial = 5 (Regra de Negócio)
	[3] = semáforo mutex para controlar entrada/saída dos senadores na sala de votação, onde seu valor inicial = número de senadores
*/
sem_t parliamentary_semaphores[TOTAL_PARLIAMENTARIANS_TYPES];

sem_t *shared_memory_semaphore;                               /* Semáforo para controle da memória compartilhada    */

/*
	Ponteiro referente a um array de inteiros que representará
	a quantidade total de membros na sala de votação, onde:

	[0] = quantidade de Senadores na sala de votação
	[1] = quantidade de Deputados na sala de votação
	[2] = quantidade de Vereadores na sala de votação
*/
int *total_parliamentarians_in_votation_room;

int read_total_parlimentary(char *parliamentary_type){
	int total_parliamentarians = 0;

	printf("Digite a quantidade de %s:\n", parliamentary_type);
	scanf("%d", &total_parliamentarians);

	while(total_parliamentarians < 1 || total_parliamentarians > 500){
		printf("Quantidade invalida de %s, digite novamente:\n", parliamentary_type);
		scanf("%d", &total_parliamentarians);
	}

	return total_parliamentarians;
}

void print_total_parliamentary_in_votation_room(){
	if(total_parliamentarians_in_votation_room[1] == MAX_DEPUTIES_IN_VOTATION_ROOM){
		printf("Numero maximo de deputados na sala atingido!\n");
	}
	
	if((total_parliamentarians_in_votation_room[0] + total_parliamentarians_in_votation_room[1] + total_parliamentarians_in_votation_room[2]) == 0 ){
		printf("A sala de votacao esta vazia!\n");
	}
	else{
		printf("Sala de votacao: [S - %d] [D - %d] [V - %d]\n", total_parliamentarians_in_votation_room[0],
			total_parliamentarians_in_votation_room[1], total_parliamentarians_in_votation_room[2]);
	}
}

/*
	Método responsável por efetuar a entrada de um parlamentar da sala de votação incrementando
	a quantidade de parlamentares na mesma.
*/
void parliamentary_entering_votation_room(char *parliamentary_type, int parliamentary_number){
	int parliamentary_position = 0;

	if(strcmp(parliamentary_type, "Senador") == 0){
		parliamentary_position = 0;
	}
	else if(strcmp(parliamentary_type, "Deputado") == 0){
		parliamentary_position = 1;
	}
	else{
		parliamentary_position = 2;
	}

	if(parliamentary_position == 0){
		total_parliamentarians_in_votation_room[parliamentary_position]++;

		printf("%s %d entrou na sala de votação!\n", parliamentary_type, parliamentary_number);

		print_total_parliamentary_in_votation_room();
	}
	else{
		// Aumentando número de parlamentares na sala de votação com base no tipo de parlamentar (Área Crítica)
		sem_wait(shared_memory_semaphore);
		total_parliamentarians_in_votation_room[parliamentary_position]++;

		printf("%s %d entrou na sala de votação!\n", parliamentary_type, parliamentary_number);

		print_total_parliamentary_in_votation_room();

		sem_post(shared_memory_semaphore);	
	}
	
}

/*
	Método responsável por efetuar a saída de um parlamentar da sala de votação diminuindo
	a quantidade de parlamentares na mesma.
*/
void parliamentary_leaving_votation_room(char *parliamentary_type, int parliamentary_number){
	int parliamentary_position = 0;

	if(strcmp(parliamentary_type, "Senador") == 0){
		parliamentary_position = 0;
	}
	else if(strcmp(parliamentary_type, "Deputado") == 0){
		parliamentary_position = 1;
	}
	else{
		parliamentary_position = 2;
	}

	if(parliamentary_position == 0){
		total_parliamentarians_in_votation_room[parliamentary_position]--;
		
		printf("%s %d saiu da sala de votação!\n", parliamentary_type, parliamentary_number);

		print_total_parliamentary_in_votation_room();
	}
	else{
		// Diminuindo número de parlamentares na sala de votação com base no tipo de parlamentar (Área Crítica)
		sem_wait(shared_memory_semaphore);
		total_parliamentarians_in_votation_room[parliamentary_position]--;
		
		printf("%s %d saiu da sala de votação!\n", parliamentary_type, parliamentary_number);

		print_total_parliamentary_in_votation_room();

		sem_post(shared_memory_semaphore);
	}
}

// Método para simular tempo do parlamentar pensando em seu voto antes de entrar na sala de votação
void parliamentary_think_vote_time(){
	int think_vote_time = rand() % (THINK_VOTE_TIME_MAX + 1 - THINK_VOTE_TIME_MIN) + THINK_VOTE_TIME_MIN;
	usleep(think_vote_time);
}

// Método responsável por realizer o controle da entrada, voto e saída de um parlamentar
void parliamentary_ready_to_enter_votation_room(int parliamentary_semaphore_position, char *parliamentary_type, 
	int parliamentary_number){

	sem_wait(&parliamentary_semaphores[parliamentary_semaphore_position]);	

	parliamentary_entering_votation_room(parliamentary_type, parliamentary_number);

	printf("%s %d realizando seu voto...\n", parliamentary_type, parliamentary_number);

	usleep(VOTE_TIME);
	
	parliamentary_leaving_votation_room(parliamentary_type, parliamentary_number);

	sem_post(&parliamentary_semaphores[parliamentary_semaphore_position]);
}

// Método de thread, responsável por preparar um senador a realizar seu voto.
void *prepare_senator_to_enter_votation_room(void *parliamentary_number){
	parliamentary_think_vote_time();

	bool room_empty = false;

	do{
		sem_wait(shared_memory_semaphore);	

		if((total_parliamentarians_in_votation_room[0] + total_parliamentarians_in_votation_room[1] + 
			total_parliamentarians_in_votation_room[2]) == 0){
			
			room_empty = true;
			parliamentary_ready_to_enter_votation_room(0, "Senador", (intptr_t) parliamentary_number);	
		}

		sem_post(shared_memory_semaphore);		
	} while(room_empty == false);


	pthread_exit(0);
}

// Método de thread, responsável por preparar um deputado a realizar seu voto.
void *prepare_deputy_to_enter_votation_room(void *deputy_number){
	parliamentary_think_vote_time();

	bool wait_senator_vote = false;

	// Verificando se existe algum senador na sala de votação (Área Crítica)
	sem_wait(shared_memory_semaphore);
	if(total_parliamentarians_in_votation_room[0] == MAX_SENATORS_IN_VOTATION_ROOM){
		wait_senator_vote = true;
	}
	sem_post(shared_memory_semaphore);

	if(wait_senator_vote == true){
		printf("Deputado %ld tentou entrar na sala e esta esperando o senador finalizar seu voto...\n", (intptr_t) deputy_number);
		
		sem_wait(&parliamentary_semaphores[0]);

		parliamentary_ready_to_enter_votation_room(1, "Deputado", (intptr_t) deputy_number);
		
		sem_post(&parliamentary_semaphores[0]);	
	}
	else{
		parliamentary_ready_to_enter_votation_room(1, "Deputado", (intptr_t) deputy_number);
	}
	
	pthread_exit(0);
}

// Método de thread, responsável por preparar um vereador a realizar seu voto.
void *prepare_alderman_to_enter_votation_room(void *alderman_number){
	parliamentary_think_vote_time();

	bool wait_senator_vote = false;
	bool room_full_of_deputies = false;

	sem_wait(shared_memory_semaphore);
	// Verificando se existe algum senador na sala de votação
	if(total_parliamentarians_in_votation_room[0] == MAX_SENATORS_IN_VOTATION_ROOM){
		wait_senator_vote = true;
	}
	// Verificando se exitem 5 deputados na sala de votação
	if(total_parliamentarians_in_votation_room[1] == MAX_DEPUTIES_IN_VOTATION_ROOM){
		room_full_of_deputies = true;
	}
	sem_post(shared_memory_semaphore);

	if(wait_senator_vote == true){
		printf("Vereador %ld tentou entrar na sala e esta esperando o senador finalizar seu voto...\n", (intptr_t) alderman_number);
		sem_wait(&parliamentary_semaphores[0]);

		parliamentary_ready_to_enter_votation_room(2, "Vereador", (intptr_t) alderman_number);

		sem_post(&parliamentary_semaphores[0]);
	}
	else if(room_full_of_deputies == true){
		printf("Vereador %ld tentou entrar na sala e esta esperando algum deputado finalizar seu voto...\n", (intptr_t) alderman_number);

		sem_wait(&parliamentary_semaphores[1]);

		parliamentary_ready_to_enter_votation_room(2, "Vereador", (intptr_t) alderman_number);

		sem_post(&parliamentary_semaphores[1]);
	}
	else{
		parliamentary_ready_to_enter_votation_room(2, "Vereador", (intptr_t) alderman_number);
	}

	pthread_exit(0);
}

// Método responsável por verificar o maior número entre o total de senadores, deputados e vereadores.
int verify_max_parliamentaries(int total_senators, int total_deputys, int total_aldermans){
	int max_parliamentaries = total_senators;

	(max_parliamentaries < total_deputys) && (max_parliamentaries = total_deputys);
	(max_parliamentaries < total_aldermans) && (max_parliamentaries = total_aldermans);

	return max_parliamentaries;
}

// Método responsável pela criação de todas as threads dos parlamentares
create_all_parliamentaries_threads(pthread_t *senators_threads, pthread_t *deputys_threads, pthread_t *aldermans_threads, 
	int total_senators, int total_deputys, int total_aldermans){
	
	int max_parliamentaries = verify_max_parliamentaries(total_senators, total_deputys, total_aldermans);

	int i = 0;

	for(i = 0; i < max_parliamentaries; i++){
		if(i < total_senators){
			pthread_create(&senators_threads[i], NULL, prepare_senator_to_enter_votation_room, (void *) (intptr_t) (i + 1));
		}

		if(i < total_deputys){
			pthread_create(&deputys_threads[i], NULL, prepare_deputy_to_enter_votation_room, (void *) (intptr_t) (i + 1));
		}

		if(i < total_aldermans){
			pthread_create(&aldermans_threads[i], NULL, prepare_alderman_to_enter_votation_room, (void *) (intptr_t) (i + 1));
		}
	}

	// Esperando threads terminarem
	for(i = 0; i < total_senators; i++){
		pthread_join(senators_threads[i], NULL);
	}

	for(i = 0; i < total_deputys; i++){
		pthread_join(deputys_threads[i], NULL);
	}

	for(i = 0; i < total_aldermans; i++){
		pthread_join(aldermans_threads[i], NULL);
	}
}