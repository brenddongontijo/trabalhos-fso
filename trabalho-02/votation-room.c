#include "library.h"

int main(){
	int total_senators = 0;   /* Total Senadores  */
	int total_deputys = 0;   /* Total Deputados  */
	int total_aldermans = 0; /* Total Vereadores */

	// Lendo do teclado total de senadores, deputados e vereadores
	total_senators = read_total_parlimentary("senadores");
	total_deputys = read_total_parlimentary("deputados");
	total_aldermans = read_total_parlimentary("vereadores");

	// Criação da memória compartilhada
	int shm_id = create_shared_memory();
	total_parliamentarians_in_votation_room = vinculate_pointer_to_shared_memory(shm_id); /* Ponteiro para memória compartilhada */

	shared_memory_semaphore = (sem_t *) malloc(sizeof(sem_t)); /* Alocando memória para semáforo da memória compartilhada */

	/* Inicializando todos os os semáforos de parlamentares e memória compartilhada (Variável Global) */
	init_all_semaphores(parliamentary_semaphores, shared_memory_semaphore, total_aldermans);

	// Inicializando n threads para senadores e deputados com base em suas respectivas quantidades.
	pthread_t *senators_threads = (pthread_t *) malloc(sizeof(pthread_t) * total_senators);
	pthread_t *deputys_threads = (pthread_t *) malloc(sizeof(pthread_t) * total_deputys);
	pthread_t *aldermans_threads = (pthread_t *) malloc(sizeof(pthread_t) * total_aldermans);

	// Criando todas as threads
	create_all_parliamentaries_threads(senators_threads, deputys_threads, aldermans_threads, total_senators, total_deputys, total_aldermans);

	// Destruindo todos semáforo
	destroy_all_semaphores(parliamentary_semaphores, shared_memory_semaphore);

	return 0;
}