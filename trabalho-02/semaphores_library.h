#include "macros.h"

// Método auxiliar para mostrar valor do semáforo passado
void print_semaphore_value(sem_t *semaphore){
	int number = 0;

	sem_getvalue(semaphore, &number);

	printf("Valor do semaforo: %d\n", number);
}

// Método para iniciar cada um dos semáforos dentro de um array de semáforos
void init_all_semaphores(sem_t *parliamentary_semaphores, sem_t *shared_memory_semaphore, int total_alderman){
	int initial_semaphore_value = 1;

	/* Posições dos 3 tipos de semáforos */
	int senator_semaphore = 0;
	int depoty_semaphore = 1;
	int alderman_semaphore = 2;

	int current_semaphore_type = 0;

	for(current_semaphore_type = 0; current_semaphore_type < TOTAL_PARLIAMENTARIANS_TYPES; current_semaphore_type++){
		if(current_semaphore_type == depoty_semaphore){
			initial_semaphore_value = MAX_DEPUTIES_IN_VOTATION_ROOM;
		}
		else if(current_semaphore_type == alderman_semaphore){
			initial_semaphore_value = total_alderman + 1;
		}
		else{
			initial_semaphore_value = 1;
		}
		
		// Criando e iniciando semáforo dos parlamentares.
		if(sem_init(&parliamentary_semaphores[current_semaphore_type], 0, initial_semaphore_value) == -1){
			perror("Problemas ao iniciar semaforo dos parlamentares!\n");
			exit(1);
		}		
	}

	initial_semaphore_value = 1;

	// Criando e iniciando semáforo para controle da memória compartilhada.
	if(sem_init(shared_memory_semaphore, 0, initial_semaphore_value) == -1){
		perror("Problemas ao iniciar semaforo para controle da memoria compartilhada!\n");
		exit(1);
	}
}

// Método responsável por destruir todos os semáforos
void destroy_all_semaphores(sem_t *parliamentary_semaphores, sem_t *shared_memory_semaphore){
	sem_destroy(&parliamentary_semaphores[0]);
	sem_destroy(&parliamentary_semaphores[1]);
	sem_destroy(&parliamentary_semaphores[2]);

	sem_destroy(shared_memory_semaphore);
}