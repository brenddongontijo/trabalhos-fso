#include "macros.h"

// Método para gerar uma chave IPC única
key_t generate_unique_ipc_key(){
	key_t key;

	if((key = ftok("/tmp", 'A')) == (key_t) -1){
		perror("Problemas ao criar chave IPC unica!\n");
		exit(1);
	}

	return key;
}

// Método para criação da memória compartilhada
int create_shared_memory(){
	key_t shm_key = generate_unique_ipc_key();

	int shm_id = 0;

	if((shm_id = shmget(shm_key, sizeof(int) * TOTAL_PARLIAMENTARIANS_TYPES, IPC_CREAT | 0666)) < 0){
		perror("Problemas ao alocar segmento de memoria compartilhada!\n");
		exit(1);
	}

	return shm_id;
}

// Método para vincular ponteiro ao espaço de memória compartilhada alocado
int *vinculate_pointer_to_shared_memory(int shm_id){
	int *total_members_in_room;

	if((total_members_in_room = shmat(shm_id, NULL, 0)) == (int *) -1){
		perror("Problemas ao vincular ponteiro de shm_message ao espaco de memoria!\n");
		exit(1);
	}

	/* Inicializando quantidade de parlamentares na sala de votação */
	total_members_in_room[0] = 0;
	total_members_in_room[1] = 0;
	total_members_in_room[2] = 0;

	return total_members_in_room;
}