#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <poll.h>
#include <semaphore.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/shm.h>
#include <stdbool.h>

#define MSG_ID 10

#define SENDER_NAME_SIZE 50

#define MSG_SIZE_TEXT 256

#define MSG_SIZEMAX 260

#define WAIT_TIME 100 // 10 seconds

#define SEM_NAME "/semaphore_name"

// Estrutura responsável por representar as mensagens enviadas para a fila de mensagens
typedef struct msgform {
	long mtype;
	char text[MSG_SIZE_TEXT];
} msgform;

// Estrutura responsável por realizar controle de leitura/escrita da memória compartilhada
typedef struct msgcontroller {
	char text[MSG_SIZE_TEXT];
	char sender_name[SENDER_NAME_SIZE];
	bool new_message;
} msgcontroller;

// Variáveis Globais
sem_t *semaphore;
struct pollfd mypoll = { STDIN_FILENO, POLLIN|POLLPRI };

void capture_signal_host_a(int signal){
	printf("\nEncerrando programa...!\n");

	// Destruindo semáforo pelo seu nome
	sem_unlink(SEM_NAME);

	exit(0);
}

void capture_signal_host_b(int signal){
	printf("\nEncerrando programa...!\n");
	int semaphore_value = 0;

	// Liberando semáforo caso o mesmo esteja fechado e o processo seja interrompido.
	if((semaphore_value = sem_getvalue(semaphore, &semaphore_value)) == 0){
		sem_post(semaphore);
	}

	exit(0);
}

// Método de controle para leitura/escrita para os filhos dos hosts A e B
void infinite_loop_controller_son(msgcontroller *shm_message, msgform *msg, int msg_id_queue_send, int msg_id_queue_response, char *current_sender_process){

	for(;;){
		// Fechando Semáforo
		sem_wait(semaphore);
		
		// Não há mensagens novas na memória compartilhada
		if(shm_message->new_message == false){
			if(msgrcv(msg_id_queue_send, msg, MSG_SIZEMAX, 1, MSG_NOERROR | IPC_NOWAIT) == -1){          
				// Não há mensagens na fila de mensagens, nada a fazer.
			}
			else{
				// Copiando conteúdo da mensagem recebida pelo pai para a memória compartilhada
				strcpy(shm_message->text, msg->text);
				msg->mtype = 1;
				shm_message->new_message = true;
				strncpy(shm_message->sender_name, current_sender_process, SENDER_NAME_SIZE);
			}
		}
		else if((shm_message->new_message == true) && (strcmp(shm_message->sender_name, current_sender_process) != 0)){
			// Copiando conteúdo da memória compartilhada para a mensagem
			strcpy(msg->text, shm_message->text);
			msg->mtype = 1;

			// Enviando mensagem de volta para processo pai
			if(msgsnd(msg_id_queue_response, msg, MSG_SIZEMAX, MSG_NOERROR) == -1){
				perror("Problemas ao enviar mensagem para processo pai!\n");
				exit(1);
			}

			shm_message->new_message = false;
		}
		
		// Liberando Semáforo
		sem_post(semaphore);
	}
}

// Método de controle para leitura/escrita para os hosts A e B
void infinite_loop_controller_father(msgform *msg, int msg_id_queue_send, int msg_id_queue_response){
	char *find_new_line_character;
	printf("Chat iniciado, digite uma mensagem...\n");

	for(;;){
		// Esperando uma entrada de teclado com base no WAIT_TIME
		if(poll(&mypoll, 1, WAIT_TIME)){
			fgets(msg->text, MSG_SIZE_TEXT, stdin);

			// Limpando quebra de linha
			if((find_new_line_character = strchr(msg->text, '\n')) != NULL){
				*find_new_line_character = '\0';
			}

			msg->mtype = 1;

			// Enviando mensagem para processo filho
			if(msgsnd(msg_id_queue_send, msg, MSG_SIZEMAX, 0) == -1){
				perror("Problemas ao enviar mensagem para processo filho!\n");
				exit(1);
			}
		}
		else{
			// Tentando receber mensagens do processo filho
			if(msgrcv(msg_id_queue_response, msg, MSG_SIZEMAX, 1, MSG_NOERROR | IPC_NOWAIT) == -1){
				// Não há mensagens, nada a fazer.
			}
			else{
				// Imprimindo conteúdo da mensagem
				printf("'%s' <<<\n", msg->text);
			}
		}
	}
}

void set_initial_shm_configuration(msgcontroller *shm_message){
	sem_wait(semaphore);

	shm_message->new_message = false;

	sem_post(semaphore);
}