#include "library.h"

int main(){
	msgform *msg = (msgform *) malloc(sizeof(msgform));
	int msg_id_queue_send = 0;
	int msg_id_queue_reception = 0;
	
	// Criando fila para envio de mensagens do host B
	if((msg_id_queue_send = msgget(MSG_ID + 2, 0666 | IPC_CREAT)) == -1){
		perror("Problemas para recuperar ID da fila de envio de mensagens!\n");
		exit(1);
	}

	// Criando fila para recebimento de mensagens do host B
	if((msg_id_queue_reception = msgget(MSG_ID + 3, 0666 | IPC_CREAT)) == -1){
		perror("Problemas para recuperar ID da fila de retorno de mensagens!\n");
		exit(1);
	}

	pid_t pid = fork();

	if(pid == 0){
		// Processo filho
		int shm_id = 0;
		key_t shm_key = 5678;
		char sender_name[SENDER_NAME_SIZE] = "son_host_B";
		msgcontroller *shm_message;

		// Recuperando semáforo criado a partir de seu nome
		if((semaphore = sem_open(SEM_NAME, 0)) == SEM_FAILED){
			perror("Problemas ao recuperar semaforo!\n");
			kill(getppid(), SIGKILL);
			exit(1);
		}

		// Recuperando id da memória compartilhada
		if((shm_id = shmget(shm_key, sizeof(msgcontroller), 0666)) < 0){
			perror("Problemas ao alocar segmento de memoria compartilhada!\n");
			exit(1);
		}

		// Vinculando ponteiro *shm_message ao espaço de memória compartilhada alocado
		if((shm_message = shmat(shm_id, NULL, 0)) == (msgcontroller *) -1){
			perror("Problemas ao vincular ponteiro de shm_message ao espaco de memoria!\n");
			exit(1);
		}

		signal(SIGINT, capture_signal_host_b);
		
		infinite_loop_controller_son(shm_message, msg, msg_id_queue_send, msg_id_queue_reception, sender_name);
	}
	else if(pid > 0){
		// Processo pai
		infinite_loop_controller_father(msg, msg_id_queue_send, msg_id_queue_reception);
	}
	else{
		perror("Problemas ao criar filho do host B!\n");
		exit(1);
	}

	free(msg);
	exit(0);
}
