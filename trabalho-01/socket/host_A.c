#include "library.h"

int main(){
	struct msgform *msg = (struct msgform *) malloc(sizeof(struct msgform));
	int msg_id_queue_send = 0;
	int msg_id_queue_response = 0;

	// Recuperando ID da fila para envio de mensagens
	if((msg_id_queue_send = msgget(MSG_ID, 0666|IPC_CREAT)) == -1){
		perror("Problemas para recuperar ID da fila de envio de mensagens!\n");
		exit(1);
	}

	// Recuperando ID da fila para retorno de mensagens
	if((msg_id_queue_response = msgget(MSG_ID+1, 0666|IPC_CREAT)) == -1){
		perror("Problemas para recuperar ID da fila de retorno de mensagens!\n");
		exit(1);
	}

	pid_t pid = fork();

	if(pid == 0){
		int sd, new_sd;
		struct sockaddr_in endServ;  /* endereço do servidor   */
   		struct sockaddr_in endCli;   /* endereço do cliente    */
		
   		memset((char *) &endServ, 0, sizeof(endServ)); /* limpa variavel endServ    */
		endServ.sin_family = AF_INET;           	/* familia TCP/IP   */
   		endServ.sin_addr.s_addr = inet_addr("127.0.0.1"); 	/* endereço IP      */
   		endServ.sin_port = htons(3100); /* PORTA	*/

		// Criando socket
		if((sd = socket(AF_INET, SOCK_STREAM, 0)) < 0){
			perror("Problemas ao criar socket!\n");
			exit(1);
		}

		if(setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, &(int){ 1 }, sizeof(int)) < 0){
    		perror("Problemas ao configurar reuso de endereco no socket!\n");
			exit(1);
		}

		// Ligando socket a porta e ip
		if(bind(sd, (struct sockaddr *) &endServ, sizeof(endServ)) < 0){
			perror("Problemas vincular socket a porta e ip!\n");
			exit(1);
		}

		// Ouvindo a porta
		if(listen(sd, 1) < 0){
			perror("Problemas ao ouvir a porta!\n");
			exit(1);
		}

		int size_client = sizeof(endCli);

		struct timeval tv;

		tv.tv_sec = 0;
		tv.tv_usec = 5;
		
		printf("Esperando host B conectar ao socket...\n");
		for(;;){
			if((new_sd = accept(sd, (struct sockaddr *) &endCli, &size_client)) < 0){
				perror("Problemas na conexao!\n");
				exit(1);
			}
			
			if(setsockopt(new_sd, SOL_SOCKET, SO_RCVTIMEO, (char *) &tv, sizeof(struct timeval)) < 0){
				perror("Problemas ao configurar timeout para rcv do socket!\n");
				exit(1);
			}

			printf("Host B conectado!\n");
			printf("Chat iniciado, digite uma mensagem:\n");

			char message_text[MSG_SIZE_TEXT];

			while(1) {
				memset(&message_text, 0x0, sizeof(message_text));

				if(msgrcv(msg_id_queue_send, msg, MSG_SIZEMAX, 1, MSG_NOERROR | IPC_NOWAIT) == -1){
					// Sem mensagens novas
					if(recv(new_sd, &message_text, sizeof(message_text), 0) > 0){
						if(strncmp(message_text, "FIM", 3) == 0){
							break;
						}
						
						strncpy(msg->text, message_text, MSG_SIZE_TEXT);
						msg->mtype = 1;

						if(msgsnd(msg_id_queue_response, msg, MSG_SIZEMAX, 0) == -1){
							perror("Problemas ao enviar mensagem para receptor!\n");
							exit(1);
						}
					}
				}
				else{
					strcpy(message_text, msg->text);
					send(new_sd, &message_text, strlen(message_text),0);
					if(strncmp(message_text, "FIM", 3) == 0){
						break;
					}
				}
			}
			close(new_sd);

			kill(getppid(), SIGKILL);
			exit(1);
		}
	}
	else if(pid > 0){
		infinite_loop_controller_father(msg, msg_id_queue_send, msg_id_queue_response);
	}
	else{
		perror("Problemas ao criar filho deste processo!\n");
		exit(1);
	}

	free(msg);
	exit(0);
}
