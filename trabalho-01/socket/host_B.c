#include "library.h"

int main(){
	struct msgform *msg = (struct msgform *) malloc(sizeof(struct msgform));
	int msg_id_queue_send = 0;
	int msg_id_queue_response = 0;
	
	// Criando fila para envio de mensagens do cliente 02
	if((msg_id_queue_send = msgget(MSG_ID + 2, 0666|IPC_CREAT)) == -1){
		perror("Problemas para recuperar ID da fila de envio de mensagens!\n");
		exit(1);
	}

	// Criando fila para envio de mensagens do cliente 02  
	if((msg_id_queue_response = msgget(MSG_ID + 3, 0666|IPC_CREAT)) == -1){
		perror("Problemas para recuperar ID da fila de retorno de mensagens!\n");
		exit(1);
	}

	pid_t pid = fork();

	if(pid == 0){
		struct  sockaddr_in ladoServ; /* contem dados do servidor */
		int     sd;              /* socket descriptor                   */
		int     n;               /* numero de caracteres lidos do servidor */
		char    message_text[MSG_SIZE_TEXT]; /* buffer de dados enviados  */
		
		memset((char *) &ladoServ, 0, sizeof(ladoServ)); /* limpa estrutura */
		memset((char *) &message_text, 0, sizeof(message_text)); /* limpa buffer */

		ladoServ.sin_family = AF_INET; /* config. socket p. internet*/
		ladoServ.sin_addr.s_addr = inet_addr("127.0.0.1"); /* ip servidor */
		ladoServ.sin_port = htons(3100); /* porta servidor */

		// Criando socket
		if((sd = socket(PF_INET, SOCK_STREAM, 0)) < 0){
			perror("Problemas ao criar socket!\n");
			exit(1);
		}

		// Conectando socket ao host A
		if(connect(sd, (struct sockaddr *) &ladoServ, sizeof(ladoServ)) < 0) {
			perror("Problemas ao conectar com host A!\n");
			exit(1);
		}

		printf("Host B conectado com sucesso ao Host A!\n");
		printf("Chat iniciado, digite uma mensagem:\n");

		// Tempo de timeout para rcv do socket
		struct timeval tv;
		tv.tv_sec = 0;
		tv.tv_usec = 5;

		if(setsockopt(sd, SOL_SOCKET, SO_RCVTIMEO, (char *) &tv, sizeof(struct timeval)) < 0){
			perror("Problemas ao configurar timeout para rcv do socket!\n");
			exit(1);
		}

		while(1) {
			memset(&message_text, 0x0, sizeof(message_text));

			if(msgrcv(msg_id_queue_send, msg, MSG_SIZEMAX, 1, MSG_NOERROR | IPC_NOWAIT) == -1){
				if(recv(sd, &message_text, sizeof(message_text), 0) > 0){
					if(strncmp(message_text, "FIM", 3) == 0){
						break;
					}
					
					strncpy(msg->text, message_text, MSG_SIZE_TEXT);
					msg->mtype = 1;

					if(msgsnd(msg_id_queue_response, msg, MSG_SIZEMAX, 0) == -1){
						perror("Problemas ao enviar mensagem para receptor!\n");
						exit(1);
					}
				}
			}
			else{
				strcpy(message_text, msg->text);
				send(sd, &message_text, strlen(message_text),0);
				if(strncmp(message_text, "FIM", 3) == 0){
					break;
				}
			}
		}

		close(sd);

		kill(getppid(), SIGKILL);
		exit(1);
	}
	else if(pid > 0){
		infinite_loop_controller_father(msg, msg_id_queue_send, msg_id_queue_response);
	}
	else{
		perror("Problemas ao criar filho deste processo!\n");
		exit(1);
	}

	free(msg);
	exit(0);
}
