#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <poll.h>
#include <semaphore.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/shm.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdbool.h>

#define MSG_ID 10

#define MSG_SIZE_TEXT 256

#define MSG_SIZEMAX 260

#define WAIT_TIME 100 // 0.1 seconds

typedef struct msgform {
	long mtype;
	int pid;
	char text[MSG_SIZE_TEXT];
} msgform;

// Variáveis Globais
struct pollfd mypoll = { STDIN_FILENO, POLLIN|POLLPRI };

// Método de controle para leitura/escrita para os hosts A e B
void infinite_loop_controller_father(msgform *msg, int msg_id_queue_send, int msg_id_queue_response){
	char *find_new_line_character;

	for(;;){
		// Esperando uma entrada de teclado com base no WAIT_TIME
		if(poll(&mypoll, 1, WAIT_TIME)){
			fgets(msg->text, MSG_SIZE_TEXT, stdin);

			// Limpando quebra de linha
			if((find_new_line_character = strchr(msg->text, '\n')) != NULL){
				*find_new_line_character = '\0';
			}

			msg->mtype = 1;

			// Enviando mensagem para processo filho
			if(msgsnd(msg_id_queue_send, msg, MSG_SIZEMAX, 0) == -1){
				perror("Problemas ao enviar mensagem para processo filho!\n");
				exit(1);
			}
		}
		else{
			// Tentando receber mensagens do processo filho
			if(msgrcv(msg_id_queue_response, msg, MSG_SIZEMAX, 1, MSG_NOERROR | IPC_NOWAIT) == -1){
				// Não há mensagens, nada a fazer.
			}
			else{
				// Imprimindo conteúdo da mensagem
				printf("'%s' <<<\n", msg->text);
			}
		}
	}
}
